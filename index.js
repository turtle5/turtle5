//
// Logo Interpreter in Javascript
//

// Copyright (C) 2011 Joshua Bell
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

var g_logo;

var g_history = [];
var g_historypos = -1;

var g_entry;
var g_multi = false;
var currentTurtle = null;

function onenter() {
    var e = g_entry;
    var v = g_entry.value;
    if (v !== '') {

        if (!g_multi) {
            g_history.push(v);
            g_historypos = -1;
        }

        try {
            g_logo.run(v);
        } catch (e) {
            window.alert("Error: " + e.message);
        }
    }
}

var KEY = {
    RETURN: 10, // iOS
    ENTER: 13,
    END: 35,
    HOME: 36,
    LEFT: 37,
    UP: 38,
    RIGHT: 39,
    DOWN: 40
};

function onkey(e) {
    e = e ? e : window.event;

    var consume = false;

    switch (e.keyCode) {
        case KEY.UP:
            if (g_history.length > 0) {
                if (g_historypos === -1) {
                    g_historypos = g_history.length - 1;
                } else {
                    g_historypos = (g_historypos === 0) ? g_history.length - 1 : g_historypos - 1;
                }
                document.getElementById('entry_multi').value = g_history[g_historypos];
            }
            consume = true;
            break;

        case KEY.DOWN:
            if (g_history.length > 0) {
                if (g_historypos === -1) {
                    g_historypos = 0;
                } else {
                    g_historypos = (g_historypos === g_history.length - 1) ? 0 : g_historypos + 1;
                }
                document.getElementById('entry_multi').value = g_history[g_historypos];
            }
            consume = true;
            break;
    }

    if (consume) {
        e.cancelBubble = true; // IE
        e.returnValue = false;
        if (e.stopPropagation) {
            e.stopPropagation();
        } // W3C
        if (e.preventDefault) {
            e.preventDefault();
        } // e.g. to block arrows from scrolling the page
        return false;
    } else {
        return true;
    }
}

var savehook;
function initStorage(loadhook) {
    var indexedDB =
            window.webkitIndexedDB || window.mozIndexedDB || window.indexedDB;
    if (!indexedDB)
        return;

    var req = indexedDB.open('logo', 2);
    req.onupgradeneeded = function() {
        var db = req.result;
        db.createObjectStore('procedures');
    };
    req.onsuccess = function() {
        var db = req.result;

        var tx = db.transaction('procedures');
        var curReq = tx.objectStore('procedures').openCursor();
        curReq.onsuccess = function() {
            var cursor = curReq.result;
            if (cursor) {
                try {
                    loadhook(cursor.value);
                } catch (e) {
                    console.log("error loading procedure: " + e);
                }
                cursor.continue();
            } else {
                savehook = function(name, def) {
                    var tx = db.transaction('procedures', 'readwrite');
                    tx.objectStore('procedures').put(def, name);
                };
            }
        };
    };
}

window.onload = function() {
    resizeCanvas(false);
    window.onresize = function() {
        resizeCanvas(true);
    };
    var stream = {
        read: function(s) {
            return window.prompt(s ? s : "");
        },
        write: function() {
            var div = document.getElementById('overlay');
            for (var i = 0; i < arguments.length; i += 1) {
                div.innerHTML += arguments[i];
            }
            div.scrollTop = div.scrollHeight;
        },
        clear: function() {
            var div = document.getElementById('overlay');
            div.innerHTML = "";
        },
        readback: function() {
            var div = document.getElementById('overlay');
            return div.innerHTML;
        }
    };

    var canvas_element = document.getElementById("sandbox"), canvas_ctx = canvas_element.getContext('2d'),
            turtle_element = document.getElementById("turtle"), turtle_ctx = turtle_element.getContext('2d');
    var turtle = new CanvasTurtle(
            canvas_ctx,
            turtle_ctx,
            canvas_element.width, canvas_element.height);
    currentTurtle = turtle;
    g_logo = new LogoInterpreter(
            turtle, stream,
            function(name, def) {
                if (savehook) {
                    savehook(name, def);
                }
            });
    initStorage(function(def) {
        g_logo.run(def);
    });

    document.getElementById('run').onclick = onenter;

    g_entry = document.getElementById('entry_multi');
    g_entry.onkeydown = onkey;
    g_entry.focus();

    function demo(param) {
        param = String(param);
        if (param.length > 0) {
            param = decodeURIComponent(param.substring(1).replace(/\_/g, ' '));
            g_entry.value = param;
            try {
                g_logo.run(param);
            } catch (e) {
                window.alert("Error: " + e.message);
            }
        }
    }

    // Look for a program to run in the query string / hash
    var param = document.location.search || document.location.hash;
    demo(param);
    window.onhashchange = function(e) {
        demo(document.location.hash);
    };
};
function resizeCanvas(centralizarSprite) {
    var cSandbox = document.getElementById("sandbox");
    var cTurtle = document.getElementById("turtle");
    var imgDataSandbox = cSandbox.getContext("2d").getImageData(0, 0, cSandbox.width, cSandbox.height);
    var imgDataTurtle = cTurtle.getContext("2d").getImageData(0, 0, cTurtle.width, cTurtle.height);
    var widthAntes = cTurtle.clientWidth;
    var heightAntes = cTurtle.clientHeight;
    var widthDepois = document.body.clientWidth;
    var heightDepois = document.body.clientHeight;
    cSandbox.width = widthDepois;
    cSandbox.height = heightDepois;
    cTurtle.width = document.body.clientWidth;
    cTurtle.height = document.body.clientHeight;
    var x = 0;
    var y = 0;
    if (widthDepois > widthAntes) {
        x = widthDepois - widthAntes;
    } else {
        x = (widthAntes - widthDepois) * -1;
    }
    if (heightDepois > heightAntes) {
        y = heightDepois - heightAntes;
    } else {
        y = (heightAntes - heightDepois) * -1;
    }
    x = x / 2;
    y = y / 2;
    if (centralizarSprite) {
        currentTurtle.x += x;
        currentTurtle.y += y;
        cTurtle.getContext("2d").putImageData(imgDataTurtle, x, y);
        cSandbox.getContext("2d").putImageData(imgDataSandbox, x, y);
        cSandbox.getContext("2d").fillColor = currentTurtle.getColor();
        cSandbox.getContext("2d").lineWidth = currentTurtle.getLineWidth();
        currentTurtle.updateColors();
    }
}
function translateCanvas(canvas, x, y) {
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var recorte = ctx.getImageData(0, 0, canvas.width, canvas.height);
    ctx.putImageData(recorte, x, y);
}
